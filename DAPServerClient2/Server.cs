﻿using DAPprotocols;
using llcss;
using Nito.AsyncEx;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace DAPServerClient2
{
    public class Server
    {
        private const int port = 10003;
        private TcpListener listener;

        private List<ClientObject> ListOfClientObjects = new List<ClientObject>();

        public delegate void ResponseNeeded(int Response, int extraCode);
        public event ResponseNeeded ResponseIsNeeded;

        public delegate void SpectrumResponseNeeded(int Response, GetSpectrumRequest getSpectrumRequest);
        public event SpectrumResponseNeeded SpectrumResponseIsNeeded;

        public delegate void CorrFuncResponseNeeded(int Response, GetCorrFuncRequest getCorrFuncRequest);
        public event CorrFuncResponseNeeded CorrFuncResponseIsNeeded;

        public delegate void DeviceGainMessageResponseNeeded(int Response, DeviceGainMessage deviceGainMessage);
        public event DeviceGainMessageResponseNeeded DeviceGainMessageResponseIsNeeded;

        public delegate void FiltersResponseNeeded(int Response, FiltersMessage filtersMessage);
        public event FiltersResponseNeeded FiltersResponseIsNeeded;

        public delegate void RangeMessageResponseNeeded(int Response, RangeMessage rangeMessage);
        public event RangeMessageResponseNeeded RangeMessageResponseIsNeeded;

        public delegate void SignalMessageResponseNeeded(int Response, SignalMessage signalMessage);
        public event SignalMessageResponseNeeded SignalMessageResponseIsNeeded;

        public delegate void ToggleStartStopMessageResponse(int Response, ToggleStartStopMessage toggleStartStopMessage);
        public event ToggleStartStopMessageResponse ToggleStartStopMessageResponseIsNeeded;

        public delegate void ExtraMessageResponseNeeded(int Response, ExtraMessage extraMessage);
        public event ExtraMessageResponseNeeded ExtraMessageResponseIsNeeded;

        Dictionary<int, int> Responses = new Dictionary<int, int>();

        List<(int, int)> Responses2 = new List<(int, int)>();

        public async Task ServerStart()
        {
            listener = new TcpListener(IPAddress.Parse("127.0.0.1"), port);
            listener.Start();
            Console.WriteLine($"Server started on {IPAddress.Parse("127.0.0.1")}:{port}");
            Console.WriteLine("Waiting for connections...");
            while (true)
            {
                TcpClient client = await listener.AcceptTcpClientAsync();
                ClientObject clientObject = new ClientObject(client);
                ListOfClientObjects.Add(clientObject);

                InitClientObjectEvents(ref clientObject);

                Console.WriteLine("Client connected");

                //создаем новый поток для обслуживания нового клиента
                Task.Run(() => clientObject.Read());
                var answer = await ServerSendAddressIdentificationRequest();
            }
        }

        public async Task ServerStart(string IP, int port)
        {
            try
            {
                //Пробуем стартовать сервер
                listener = new TcpListener(IPAddress.Parse(IP), port);
                listener.Start();
                Console.WriteLine($"Server started on {IP}:{port}");
                Console.WriteLine("Waiting for connections...");
            }
            catch
            {
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), port);
                listener.Start();
                Console.WriteLine($"Server started on {IPAddress.Parse("127.0.0.1")}:{port}");
                Console.WriteLine("Waiting for connections...");
            }
            while (true)
            {
                TcpClient client = await listener.AcceptTcpClientAsync();
                ClientObject clientObject = new ClientObject(client);
                ListOfClientObjects.Add(clientObject);

                InitClientObjectEvents(ref clientObject);

                Console.WriteLine("Client connected");

                //создаем новый поток для обслуживания нового клиента
                Task.Run(() => clientObject.Read());
                var answer = await ServerSendAddressIdentificationRequest();
            }
        }

        private void InitClientObjectEvents(ref ClientObject clientObject)
        {
            clientObject.ClientAdressResponse += clientObject_ClientAdressResponse;

            clientObject.ClientAdressSpectrumResponse += ClientObject_ClientAdressSpectrumResponse;
            clientObject.ClientAdressCorrFuncResponse += ClientObject_ClientAdressCorrFuncResponse;

            clientObject.ClientAdressDeviceGainMessageResponse += ClientObject_ClientAdressDeviceGainMessageResponse;

            clientObject.ClientAdressAndFiltersResponse += ClientObject_ClientAdressAndFiltersResponse;

            clientObject.ClientAdressRangeMessageResponse += ClientObject_ClientAdressRangeMessageResponse;

            clientObject.ClientAdressSignalMessageResponse += ClientObject_ClientAdressSignalMessageResponse;

            clientObject.ClientAdressAndToggleStartStopMessageResponse += ClientObject_ClientAdressAndToggleStartStopMessageResponse;

            clientObject.ClientAdressExtraMessageResponse += ClientObject_ClientAdressExtraMessageResponse;
        }

        private void clientObject_ClientAdressResponse(int Adress, int Response, int extraCode)
        {
            if (ResponseIsNeeded != null)
            {
                lock (Responses2)
                {
                    Responses2.Add((Response, Adress));
                }
                ResponseIsNeeded(Response, extraCode);
            }
        }

        private void ClientObject_ClientAdressSpectrumResponse(int Adress, int Response, GetSpectrumRequest getSpectrumRequest)
        {
            if (SpectrumResponseIsNeeded != null)
            {
                lock (Responses2)
                {
                    Responses2.Add((Response, Adress));
                }
                SpectrumResponseIsNeeded(Response, getSpectrumRequest);
            }
        }

        private void ClientObject_ClientAdressCorrFuncResponse(int Adress, int Response, GetCorrFuncRequest getCorrFuncRequest)
        {
            if (CorrFuncResponseIsNeeded != null)
            {
                lock (Responses2)
                {
                    Responses2.Add((Response, Adress));
                }
                CorrFuncResponseIsNeeded(Response, getCorrFuncRequest);
            }
        }

        private void ClientObject_ClientAdressDeviceGainMessageResponse(int Adress, int Response, DeviceGainMessage deviceGainMessage)
        {
            if (DeviceGainMessageResponseIsNeeded != null)
            {
                lock (Responses2)
                {
                    Responses2.Add((Response, Adress));
                }
                DeviceGainMessageResponseIsNeeded(Response, deviceGainMessage);
            }
        }

        private void ClientObject_ClientAdressAndFiltersResponse(int Adress, int Response, FiltersMessage filtersMessage)
        {
            if (FiltersResponseIsNeeded != null)
            {
                lock (Responses2)
                {
                    Responses2.Add((Response, Adress));
                }
                FiltersResponseIsNeeded(Response, filtersMessage);
            }
        }

        private void ClientObject_ClientAdressRangeMessageResponse(int Adress, int Response, RangeMessage rangeMessage)
        {
            if (RangeMessageResponseIsNeeded != null)
            {
                lock (Responses2)
                {
                    Responses2.Add((Response, Adress));
                }
                RangeMessageResponseIsNeeded(Response, rangeMessage);
            }
        }

        private void ClientObject_ClientAdressSignalMessageResponse(int Adress, int Response, SignalMessage signalMessage)
        {
            if (RangeMessageResponseIsNeeded != null)
            {
                lock (Responses2)
                {
                    Responses2.Add((Response, Adress));
                }
                SignalMessageResponseIsNeeded(Response, signalMessage);
            }
        }

        private void ClientObject_ClientAdressAndToggleStartStopMessageResponse(int Adress, int Response, ToggleStartStopMessage toggleStartStopMessage)
        {
            if (RangeMessageResponseIsNeeded != null)
            {
                lock (Responses2)
                {
                    Responses2.Add((Response, Adress));
                }
                ToggleStartStopMessageResponseIsNeeded(Response, toggleStartStopMessage);
            }
        }

        private void ClientObject_ClientAdressExtraMessageResponse(int Adress, int Response, ExtraMessage extraMessage)
        {
            if (RangeMessageResponseIsNeeded != null)
            {
                lock (Responses2)
                {
                    Responses2.Add((Response, Adress));
                }
                ExtraMessageResponseIsNeeded(Response, extraMessage);
            }
        }

        //Шифр 0
        public async Task<List<DefaultMessage>> ServerSendAddressIdentificationRequest()
        {
            List<DefaultMessage> ListOfDefaultMessages = new List<DefaultMessage>();
            for (int i = 0; i < ListOfClientObjects.Count; i++)
            {
                var result = await ListOfClientObjects[i].SendAddressIdentificationRequest(Convert.ToByte(i + 1));
                ListOfDefaultMessages.Add(result);
            }
            return ListOfDefaultMessages;
        }

        //Шифр 1
        public async Task<DefaultMessage> ServerSendModeMessage(DapServerMode Mode)
        {
            int i = 0;
            lock (Responses2)
            {
                var el = Responses2.First(x => x.Item1 == 1);
                i = el.Item2 - 1;
                Responses2.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendModeMessage(Mode);
            return result;
        }

        //Шифр 2
        public async Task<DefaultMessage> ServerSendFrequencyMessage()
        {
            int i = 0;
            lock (Responses2)
            {
                var el = Responses2.First(x => x.Item1 == 2);
                i = el.Item2 - 1;
                Responses2.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendFrequencyMessage();
            return result;
        }

        //Шифр 3
        public async Task<DefaultMessage> ServerSendSpectrumResponse(byte PostNumber, ChannelPicker Picker, float MinFrequencyMHz, float MaxFrequencyMHz, float[] Spectrum)
        {
            int i = 0;
            lock (Responses2)
            {
                var el = Responses2.First(x => x.Item1 == 3);
                i = el.Item2 - 1;
                Responses2.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendSpectrumResponse(PostNumber, Picker, MinFrequencyMHz, MaxFrequencyMHz, Spectrum);
            return result;
        }

        //Шифр 4
        public async Task<DefaultMessage> ServerSendCorrFuncResponse(byte FuncNumber, int FrequencykHz, int BandwidtkHz, double[] CorrFunc0, double[] CorrFunc1, double[] CorrFunc2, double[] CorrFunc3)
        {
            int i = 0;
            lock (Responses2)
            {
                var el = Responses2.First(x => x.Item1 == 4);
                i = el.Item2 - 1;
                Responses2.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendCorrFuncResponse(FuncNumber, FrequencykHz, BandwidtkHz, CorrFunc0, CorrFunc1, CorrFunc2, CorrFunc3);
            return result;
        }

        //Шифр 4
        public async Task<DefaultMessage> ServerSendCorrFunc(byte FuncNumber, int FrequencykHz, int BandwidtkHz, double[] CorrFunc0, double[] CorrFunc1, double[] CorrFunc2, double[] CorrFunc3)
        {
            DefaultMessage result = null;
            for (int i = 0; i < ListOfClientObjects.Count; i++)
            {
                result = await ListOfClientObjects[i].SendCorrFuncResponse(FuncNumber, FrequencykHz, BandwidtkHz, CorrFunc0, CorrFunc1, CorrFunc2, CorrFunc3);
            }
            return result;
        }

        //Шифр 5
        public async Task<DefaultMessage> ServerAnswerModeMessage(DapServerMode Mode)
        {
            int i = 0;
            lock (Responses2)
            {
                var el = Responses2.First(x => x.Item1 == 5);
                i = el.Item2 - 1;
                Responses2.Remove(el);
            }

            var result = await ListOfClientObjects[i].AnswerModeMessage(Mode);
            return result;
        }

        //Шифр 6
        public async Task<DefaultMessage> ServerAnswerSetFilterMessage(short Threshold, ThresholdType Type)
        {
            int i = 0;
            lock (Responses2)
            {
                var el = Responses2.First(x => x.Item1 == 6);
                i = el.Item2 - 1;
                Responses2.Remove(el);
            }

            var result = await ListOfClientObjects[i].AnswerSetFiltersMessage(Threshold, Type);
            return result;
        }

        //Шифр 7
        public async Task<DefaultMessage> ServerAnswerFiltersMessage(short Threshold, ThresholdType Type)
        {
            int i = 0;
            lock (Responses2)
            {
                var el = Responses2.First(x => x.Item1 == 7);
                i = el.Item2 - 1;
                Responses2.Remove(el);
            }

            var result = await ListOfClientObjects[i].AnswerFiltersMessage(Threshold, Type);
            return result;
        }

        //Шифр 8
        public async Task<DefaultMessage> ServerAnswerAppleExFilterMessage(bool Value)
        {
            int i = 0;
            lock (Responses2)
            {
                var el = Responses2.First(x => x.Item1 == 8);
                i = el.Item2 - 1;
                Responses2.Remove(el);
            }

            var result = await ListOfClientObjects[i].AnswerApplyExFilterMessage(Value);
            return result;
        }

        //Шифр 9
        public async Task<DefaultMessage> ServerAnswerToggleStartStopMessage(bool Value, ToggleMode toggleMode)
        {
            int i = 0;
            lock (Responses2)
            {
                var el = Responses2.First(x => x.Item1 == 9);
                i = el.Item2 - 1;
                Responses2.Remove(el);
            }

            var result = await ListOfClientObjects[i].AnswerToggleStartStopMessage(Value, toggleMode);
            return result;
        }

        //Шифр 10
        public async Task<DefaultMessage> ServerSendCoords(double Lat, double Lon, double Alt)
        {
            DefaultMessage result = null;
            for (int i = 0; i < ListOfClientObjects.Count; i++)
            {
                result = await ListOfClientObjects[i].SendCoords(Lat, Lon, Alt);
            }
            return result;
        }

        //Шифр 11
        public async Task<DefaultMessage> ServerSendExtraordinaryModeMessage(DapServerMode Mode)
        {
            DefaultMessage result = null;
            for (int i = 0; i < ListOfClientObjects.Count; i++)
            {
                result = await ListOfClientObjects[i].SendExtraordinaryModeMessage(Mode);
            }
            return result;
        }

        //Шифр 12
        public async Task<DefaultMessage> ServerAnswerSetDeviceGain(DapDevice Device, byte EPO, byte Gain)
        {
            int i = 0;
            lock (Responses2)
            {
                var el = Responses2.First(x => x.Item1 == 12);
                i = el.Item2 - 1;
                Responses2.Remove(el);
            }

            var result = await ListOfClientObjects[i].AnswerSetDeviceGain(Device, EPO, Gain);
            return result;
        }

        //Шифр 13
        public async Task<DefaultMessage> ServerAnswerGetDeviceGain(DapDevice Device, byte EPO, byte Gain)
        {
            int i = 0;
            lock (Responses2)
            {
                var el = Responses2.First(x => x.Item1 == 13);
                i = el.Item2 - 1;
                Responses2.Remove(el);
            }

            var result = await ListOfClientObjects[i].AnswerGetDeviceGain(Device, EPO, Gain);
            return result;
        }

        //Шифр 14
        public async Task<DefaultMessage> ServerSendRangeMessage()
        {
            int i = 0;
            lock (Responses2)
            {
                var el = Responses2.First(x => x.Item1 == 14);
                i = el.Item2 - 1;
                Responses2.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendRangeMessage();
            return result;
        }

        //Шифр 15
        public async Task<DefaultMessage> ServerSendSignalMessage()
        {
            int i = 0;
            lock (Responses2)
            {
                var el = Responses2.First(x => x.Item1 == 15);
                i = el.Item2 - 1;
                Responses2.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendSignalMessage();
            return result;
        }

        //Шифр 16
        public async Task<DefaultMessage> ServerSendTaus(double tau12, double tau13, double tau14)
        {
            DefaultMessage result = null;
            for (int i = 0; i < ListOfClientObjects.Count; i++)
            {
                result = await ListOfClientObjects[i].SendTaus(tau12, tau13, tau14);
            }
            return result;
        }

        //Шифр 17
        public async Task<DefaultMessage> ServerSendExtraMessage()
        {
            int i = 0;
            lock (Responses2)
            {
                var el = Responses2.First(x => x.Item1 == 17);
                i = el.Item2 - 1;
                Responses2.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendExtraMessage();
            return result;
        }
    }

    public class ClientObject
    {
        public TcpClient client;

        private readonly AsyncLock asyncLock;

        private readonly ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>> concurrentDictionary;

        public delegate void IsConnectedEventHandler(bool isConnected);
        public event IsConnectedEventHandler ClientIsConnected;

        public delegate void OnReadEventHandler(bool isRead);
        public event OnReadEventHandler ClientIsRead;

        public delegate void IsWriteEventHandler(bool isWrite);
        public event IsWriteEventHandler ClientIsWrite;

        public delegate void AdressAndResponse(int Adress, int Response, int extraCode);
        public event AdressAndResponse ClientAdressResponse;

        public delegate void AdressAndSpectrumResponse(int Adress, int Response, GetSpectrumRequest getSpectrumRequest);
        public event AdressAndSpectrumResponse ClientAdressSpectrumResponse;

        public delegate void AdressAndCorrFuncResponse(int Adress, int Response, GetCorrFuncRequest getCorrFuncRequest);
        public event AdressAndCorrFuncResponse ClientAdressCorrFuncResponse;

        public delegate void DeviceGainMessageResponse(int Adress, int Response, DeviceGainMessage deviceGainMessage);
        public event DeviceGainMessageResponse ClientAdressDeviceGainMessageResponse;

        public delegate void AdressAndFiltersResponse(int Adress, int Response, FiltersMessage filtersMessage);
        public event AdressAndFiltersResponse ClientAdressAndFiltersResponse;

        public delegate void AdressAndRangeMessageResponse(int Adress, int Response, RangeMessage rangeMessage);
        public event AdressAndRangeMessageResponse ClientAdressRangeMessageResponse;

        public delegate void AdressAndSignalMessageResponse(int Adress, int Response, SignalMessage signalMessage);
        public event AdressAndSignalMessageResponse ClientAdressSignalMessageResponse;

        public delegate void AdressAndToggleStartStopMessageResponse(int Adress, int Response, ToggleStartStopMessage toggleStartStopMessage);
        public event AdressAndToggleStartStopMessageResponse ClientAdressAndToggleStartStopMessageResponse;

        public delegate void AdressAndExtraMessageResponse(int Adress, int Response, ExtraMessage extraMessage);
        public event AdressAndExtraMessageResponse ClientAdressExtraMessageResponse;

        public int ClientAdress = 0;

        public ClientObject(TcpClient tcpClient)
        {
            client = tcpClient;
            asyncLock = new AsyncLock();
            concurrentDictionary = new ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>>();
        }


        public async Task Read()
        {
            while (client.Connected)
            {
                var headerBuffer = new byte[MessageHeader.BinarySize];
                var header = new MessageHeader();
                var count = 0;

                try
                {
                    count = await client.GetStream().ReadAsync(headerBuffer, 0, MessageHeader.BinarySize).ConfigureAwait(false);
                    if (ClientIsRead != null)
                        ClientIsRead(true);
                }
                catch (Exception)
                {
                    //Console.WriteLine("Сервер отвалился");
                    if (ClientIsRead != null)
                        ClientIsRead(false);
                    if (ClientIsConnected != null)
                        ClientIsConnected(false);
                    //break;
                }
                if (count != MessageHeader.BinarySize)
                {
                    //fatal error
                    //break;
                    if (ClientIsRead != null)
                        ClientIsRead(false);
                    return;
                }

                MessageHeader.TryParse(headerBuffer, out header);

                switch (header.Code)
                {
                    case 0:
                        Console.WriteLine(headerBuffer);
                        break;
                    case 1:
                        var response1 = await ReadResponse<ModeMessage>(header, headerBuffer).ConfigureAwait(false);
                        ClientAdressResponse?.Invoke(ClientAdress, header.Code, Convert.ToInt32(response1.Mode));
                        break;
                    case 2:
                        var response2 = await ReadResponse<FrequencyMessage>(header, headerBuffer).ConfigureAwait(false);
                        ClientAdressResponse?.Invoke(ClientAdress, header.Code, response2.Frequency);
                        break;
                    case 3:
                        var response3 = await ReadResponse<GetSpectrumRequest>(header, headerBuffer).ConfigureAwait(false);
                        ClientAdressSpectrumResponse?.Invoke(ClientAdress, header.Code, response3);
                        break;
                    case 4:
                        var response4 = await ReadResponse<GetCorrFuncRequest>(header, headerBuffer).ConfigureAwait(false);
                        ClientAdressCorrFuncResponse?.Invoke(ClientAdress, header.Code, response4);
                        break;
                    case 5:
                        var response5 = await ReadResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        ClientAdressResponse?.Invoke(ClientAdress, header.Code, 0);
                        break;
                    case 6:
                        var response6 = await ReadResponse<FiltersMessage>(header, headerBuffer).ConfigureAwait(false);
                        ClientAdressAndFiltersResponse?.Invoke(ClientAdress, header.Code, response6);
                        break;
                    case 7:
                        var response7 = await ReadResponse<FiltersMessage>(header, headerBuffer).ConfigureAwait(false);
                        ClientAdressAndFiltersResponse?.Invoke(ClientAdress, header.Code, response7);
                        break;
                    case 8:
                        var response8 = await ReadResponse<ApplyExFilterMessage>(header, headerBuffer).ConfigureAwait(false);
                        ClientAdressResponse?.Invoke(ClientAdress, header.Code, Convert.ToInt32(response8.Value));
                        break;
                    case 9:
                        var response9 = await ReadResponse<ToggleStartStopMessage>(header, headerBuffer).ConfigureAwait(false);
                        ClientAdressAndToggleStartStopMessageResponse?.Invoke(ClientAdress, header.Code, response9);
                        break;
                    case 12:
                        var response12 = await ReadResponse<DeviceGainMessage>(header, headerBuffer).ConfigureAwait(false);
                        ClientAdressDeviceGainMessageResponse?.Invoke(ClientAdress, header.Code, response12);
                        break;
                    case 13:
                        var response13 = await ReadResponse<DeviceGainMessage>(header, headerBuffer).ConfigureAwait(false);
                        ClientAdressDeviceGainMessageResponse?.Invoke(ClientAdress, header.Code, response13);
                        break;
                    case 14:
                        var response14 = await ReadResponse<RangeMessage>(header, headerBuffer).ConfigureAwait(false);
                        ClientAdressRangeMessageResponse?.Invoke(ClientAdress, header.Code, response14);
                        break;
                    case 15:
                        var response15 = await ReadResponse<SignalMessage>(header, headerBuffer).ConfigureAwait(false);
                        ClientAdressSignalMessageResponse?.Invoke(ClientAdress, header.Code, response15);
                        break;
                    case 17:
                        var response17 = await ReadResponse<ExtraMessage>(header, headerBuffer).ConfigureAwait(false);
                        ClientAdressExtraMessageResponse?.Invoke(ClientAdress, header.Code, response17);
                        break;
                    default:
                        //вывод полезной информации
                        break;
                }
            }
        }


        private async Task<T> ReadResponse<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var count = 0;
            var buffer = new byte[MessageHeader.BinarySize + header.InformationLength];
            headerBuffer.CopyTo(buffer, 0);

            if (header.InformationLength != 0)
            {
                var difference = header.InformationLength;
                var shiftpos = 0;

                while (difference != 0)
                {
                    count = await client.GetStream().ReadAsync(buffer, MessageHeader.BinarySize + shiftpos, difference).ConfigureAwait(false);
                    if (ClientIsRead != null)
                        ClientIsRead(true);
                    shiftpos += count;
                    difference = difference - count;
                }
            }

            try
            {
                var result = new T();
                result.Decode(buffer, 0);
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        static MessageHeader GetMessageHeader(int code, int length)
        {
            return new MessageHeader(0, 1, (byte)code, 0, length);
        }

        private async Task SendDefault(MessageHeader header)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                try
                {
                    await client.GetStream().WriteAsync(header.GetBytes(), 0, MessageHeader.BinarySize).ConfigureAwait(false);
                    if (ClientIsWrite != null)
                        ClientIsWrite(true);
                }
                catch (Exception)
                {
                    if (ClientIsWrite != null)
                        ClientIsWrite(false);
                }
            }
        }

        private async Task SendRequest(MessageHeader header, byte[] message)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                int count = message.Count();
                try
                {
                    await client.GetStream().WriteAsync(message, 0, count).ConfigureAwait(false);
                    if (ClientIsWrite != null)
                        ClientIsWrite(true);
                }
                catch (Exception)
                {
                    if (ClientIsWrite != null)
                        ClientIsWrite(false);
                }
            }
        }

        private async Task SendResponse<T>(T Response) where T : class, IBinarySerializable, new()
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                try
                {
                    await client.GetStream().WriteAsync(Response.GetBytes(), 0, Response.StructureBinarySize).ConfigureAwait(false);
                    if (ClientIsWrite != null)
                        ClientIsWrite(true);
                }
                catch (Exception)
                {
                    if (ClientIsWrite != null)
                        ClientIsWrite(false);
                }
            }
        }

        //Шифр 0 
        public async Task<DefaultMessage> SendAddressIdentificationRequest(byte ReceiverAddress)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                ClientAdress = ReceiverAddress;

                MessageHeader header = GetMessageHeader(code: 0, length: 0);
                header.ReceiverAddress = ReceiverAddress;
                byte[] message = DefaultMessage.ToBinary(header);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 0, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 1 
        public async Task<DefaultMessage> SendModeMessage(DapServerMode Mode)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(1, 1);
                byte[] message = ModeMessage.ToBinary(header, Mode);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 1, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 2
        public async Task<DefaultMessage> SendFrequencyMessage()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(2, 0);
                byte[] message = DefaultMessage.ToBinary(header);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 2, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 3 
        public async Task<DefaultMessage> SendSpectrumResponse(byte PostNumber, ChannelPicker Picker, float MinFrequencyMHz, float MaxFrequencyMHz, float[] Spectrum)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(3, 1 + 1 + 4 + 4 + 4 * Spectrum.Length);
                byte[] message = GetSpectrumResponse.ToBinary(header, PostNumber, Picker, MinFrequencyMHz, MaxFrequencyMHz, Spectrum);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 3, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 4 
        public async Task<DefaultMessage> SendCorrFuncResponse(byte FuncNumber, int FrequencykHz, int BandwidtkHz, double[] CorrFunc0, double[] CorrFunc1, double[] CorrFunc2, double[] CorrFunc3)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(4, 1 + 4 + 4 + 4 * 8 * CorrFunc0.Count());
                byte[] message = GetCorrFuncResponse.ToBinary(header, FuncNumber, FrequencykHz, BandwidtkHz, CorrFunc0, CorrFunc1, CorrFunc2, CorrFunc3);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(GetMessageHeader(4, 0));
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 4, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 5
        public async Task<DefaultMessage> AnswerModeMessage(DapServerMode Mode)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(5, 1);
                byte[] message = ModeMessage.ToBinary(header, Mode);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 5, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 6
        public async Task<DefaultMessage> AnswerSetFiltersMessage(short Threshold, ThresholdType Type)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(6, 2 + 1);
                byte[] message = FiltersMessage.ToBinary(header, Threshold, Type);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 6, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 7
        public async Task<DefaultMessage> AnswerFiltersMessage(short Threshold, ThresholdType Type)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(7, 2 + 1);
                byte[] message = FiltersMessage.ToBinary(header, Threshold, Type);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 7, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 8
        public async Task<DefaultMessage> AnswerApplyExFilterMessage(bool Value)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(8, 1);
                byte[] message = ApplyExFilterMessage.ToBinary(header, Value);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 8, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 9
        public async Task<DefaultMessage> AnswerToggleStartStopMessage(bool Value, ToggleMode toggleMode)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(9, 1 + 1);
                byte[] message = ToggleStartStopMessage.ToBinary(header, Value, toggleMode);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 9, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 10
        public async Task<DefaultMessage> SendCoords(double Lat, double Lon, double Alt)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(10, 8 + 8 + 8);
                byte[] message = CoordsMessage.ToBinary(header, Lat, Lon, Alt);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(GetMessageHeader(10, 0));
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 10, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 11
        public async Task<DefaultMessage> SendExtraordinaryModeMessage(DapServerMode Mode)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(11, 1);
                byte[] message = ModeMessage.ToBinary(header, Mode);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(GetMessageHeader(11, 0));
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 11, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 12
        public async Task<DefaultMessage> AnswerSetDeviceGain(DapDevice Device, byte EPO, byte Gain)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(12, 3);
                byte[] message = DeviceGainMessage.ToBinary(header, Device, EPO, Gain);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 12, 0, 3);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 13
        public async Task<DefaultMessage> AnswerGetDeviceGain(DapDevice Device, byte EPO, byte Gain)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(13, 3);
                byte[] message = DeviceGainMessage.ToBinary(header, Device, EPO, Gain);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 13, 0, 3);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 14
        public async Task<DefaultMessage> SendRangeMessage()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(14, 0);
                byte[] message = DefaultMessage.ToBinary(header);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 14, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 15
        public async Task<DefaultMessage> SendSignalMessage()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(15, 0);
                byte[] message = DefaultMessage.ToBinary(header);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 15, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 16
        public async Task<DefaultMessage> SendTaus(double tau12, double tau13, double tau14)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(16, 8 + 8 + 8);
                byte[] message = TausMessage.ToBinary(header, tau12, tau13, tau14);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(GetMessageHeader(16, 0));
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 16, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 17
        public async Task<DefaultMessage> SendExtraMessage()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(17, 0);
                byte[] message = DefaultMessage.ToBinary(header);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 17, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }
    }
}
