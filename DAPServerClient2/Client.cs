﻿using DAPprotocols;
using llcss;
using Nito.AsyncEx;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace DAPServerClient2
{
    public class Client
    {
        private TcpClient client;
        private string IP;
        private const int port = 10003;

        private byte ReceiverAddress;

        private readonly AsyncLock asyncLock;

        private readonly ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>> concurrentDictionary;

        private int _AwaiterTime = 400;
        public int AwaiterTime
        {
            get { return _AwaiterTime; }
            set
            {
                if (_AwaiterTime != value)
                    _AwaiterTime = value;
            }
        }

        bool disconnect = false;

        public delegate void IsConnectedEventHandler(bool isConnected);
        public event IsConnectedEventHandler IsConnected;

        public delegate void OnReadEventHandler(bool isRead);
        public event OnReadEventHandler IsRead;

        public delegate void IsWriteEventHandler(bool isWrite);
        public event IsWriteEventHandler IsWrite;

        public Client()
        {
            asyncLock = new AsyncLock();
            concurrentDictionary = new ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>>();
        }

        static byte OwnServerAddress = 0;
        static MessageHeader GetMessageHeader(int code, int length)
        {
            //return new MessageHeader(0, 1, (byte)code, 0, length);
            return new MessageHeader(OwnServerAddress, 1, (byte)code, 0, length);
        }

        public async Task ConnectToServer(string IP)
        {
            disconnect = false;
            client = new TcpClient();

            while (!client.Connected)
            {
                try
                {
                    await client.ConnectAsync(IPAddress.Parse(IP), port).ConfigureAwait(false);

                    this.IP = IP;
                    //this.port = port;
                }
                catch (Exception)
                {

                }
                if (!client.Connected)
                    await Task.Delay(500);
            }

            Console.WriteLine("Connect");

            await OnConnected();

            Task.Run(() => ReadThread());

            IsConnected?.Invoke(true);
        }
        public async Task ConnectToServer2(string IP, CancellationToken token)
        {
            disconnect = false;
            client = new TcpClient();

            while (!client.Connected)
            {
                if (token.IsCancellationRequested)
                    return;
                try
                {
                    await client.ConnectAsync(IPAddress.Parse(IP), port).ConfigureAwait(false);

                    this.IP = IP;
                    //this.port = port;
                }
                catch (Exception)
                {

                }
                if (!client.Connected)
                    await Task.Delay(500);
            }

            Console.WriteLine("Connect2");

            await OnConnected();

            Task.Run(() => ReadThread());

            IsConnected?.Invoke(true);
        }

        public async Task<string> IQConnectToServer(string IP, int timeout = 3000)
        {
            Regex regex = new Regex(@"^(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])){3}$");
            if (regex.IsMatch(IP, 0))
            {
                CancellationTokenSource cts = new CancellationTokenSource();
                CancellationToken token = cts.Token;

                var task = ConnectToServer2(IP, token);

                if (await Task.WhenAny(task, Task.Delay(timeout)) == task)
                {
                    Console.WriteLine("task completed within timeout");
                    return "OK";
                }
                else
                {
                    cts.Cancel();
                    Console.WriteLine("timeout logic");
                    return "Invalid IP address or Server not found";
                }
            }
            else
            {
                return "Invalid IP address";
            }
        }

        public void DisconnectFromServer()
        {
            disconnect = true;
            if (client != null)
                client.Close();
            IsConnected?.Invoke(false);
        }

        private async Task OnConnected()
        {
            var taskQueues = concurrentDictionary.Values.ToArray();
            foreach (var queue in taskQueues)
            {
                foreach (var task in queue)
                {
                    //task.SetCanceled();
                    task.SetResult(null);
                }
            }
            concurrentDictionary.Clear();
        }

        //Core
        private async Task ReadThread()
        {
            while (client.Connected)
            {
                var headerBuffer = new byte[MessageHeader.BinarySize];
                var header = new MessageHeader();
                var count = 0;

                try
                {
                    count = await client.GetStream().ReadAsync(headerBuffer, 0, MessageHeader.BinarySize).ConfigureAwait(false);
                    IsRead?.Invoke(true);
                }
                catch (Exception)
                {
                    //Console.WriteLine("Сервер отвалился");
                    IsRead?.Invoke(false);
                    IsConnected?.Invoke(false);
                    if (disconnect == false)
                        Task.Run(() => ConnectToServer(IP));
                }
                if (count != MessageHeader.BinarySize)
                {
                    //fatal error
                    //break;
                    IsRead?.Invoke(false);
                    return;
                }

                var b00l = MessageHeader.TryParse(headerBuffer, out header);
                if (b00l == false) Console.WriteLine("AWP: Critical Error: .TryParse");

                switch (header.Code)
                {
                    case 0:
                        ReceiverAddress = header.ReceiverAddress;
                        break;
                    case 1:
                        await HandleResponse<ModeMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 2:
                        await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 3:
                        await HandleResponse<GetSpectrumResponse>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 4:
                        await HandleEvent<GetCorrFuncResponse>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 5:
                        await HandleResponse<ModeMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 6:
                        await HandleResponse<FiltersMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 7:
                        await HandleResponse<FiltersMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 8:
                        await HandleResponse<ApplyExFilterMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 9:
                        await HandleResponse<ToggleStartStopMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 10:
                        await HandleEvent<CoordsMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 11:
                        await HandleEvent<ModeMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 12:
                        await HandleResponse<DeviceGainMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 13:
                        await HandleResponse<DeviceGainMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 14:
                        await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 15:
                        await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 16:
                        await HandleEvent<TausMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    default:
                        Console.WriteLine("AWP: Unknown Code: " + header.Code);
                        await ReadResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                }
            }
        }

        private async Task HandleResponse<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var response = await ReadResponse<T>(header, headerBuffer).ConfigureAwait(false);
            var taskCompletionSource = new TaskCompletionSource<IBinarySerializable>();
            if (!concurrentDictionary[header.Code].TryDequeue(out taskCompletionSource))
            {
                throw new Exception("Response came, but no one is waiting for it");
            }
            if (response != null)
            {
                taskCompletionSource.SetResult(response);
            }
            else
            {
                taskCompletionSource.SetResult(null);
                //taskCompletionSource.SetException(new Exception("Can't execute request"));
            }
        }

        private async Task<T> ReadResponse<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var count = 0;
            var added = IsShaperCommand() ? 3 : 0;
            var buffer = new byte[MessageHeader.BinarySize + header.InformationLength + added];
            headerBuffer.CopyTo(buffer, 0);

            if (header.InformationLength != 0)
            {
                var difference = header.InformationLength + added;

                var shiftpos = 0;

                try
                {
                    while (difference != 0)
                    {
                        count = await client.GetStream().ReadAsync(buffer, MessageHeader.BinarySize + shiftpos, difference).ConfigureAwait(false);
                        if (IsRead != null)
                            IsRead(true);
                        shiftpos += count;
                        difference = difference - count;
                    }
                }
                catch { return null; }
            }

            //if (await client.GetStream().ReadAsync(buffer, MessageHeader.BinarySize, header.InformationLength).ConfigureAwait(false) != header.InformationLength)
            //{
            //    return null;
            //}
            try
            {
                var result = new T();
                result.Decode(buffer, 0);
                return result;
            }
            catch (Exception e)
            {
                var с = e.ToString();
                return null;
            }

            bool IsShaperCommand() =>
                header.Code == 170 ||
                header.Code == 173 ||
                header.Code == 174;
        }

        #region Events

        //Шифр 4
        public delegate void GetCorrFuncUpdateEventHandler(GetCorrFuncResponse corrFuncResponse);
        public event GetCorrFuncUpdateEventHandler GetCorrFuncUpdate;

        //Шифр 10
        public delegate void GetCoordsMessageUpdateEventHandler(CoordsMessage coordsMessage);
        public event GetCoordsMessageUpdateEventHandler GetCoordsMessageUpdate;

        //Шифр 11
        public delegate void GetExtraordinaryModeMessageUpdateEventHandler(ModeMessage modeMessage);
        public event GetExtraordinaryModeMessageUpdateEventHandler GetExtraordinaryModeMessageUpdate;

        //Шифр 16
        public delegate void GetTausMessageUpdateEventHandler(TausMessage tausMessage);
        public event GetTausMessageUpdateEventHandler GetTausMessageUpdate;

        #endregion

        private async Task HandleEvent<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var responseEvent = await ReadResponse<T>(header, headerBuffer).ConfigureAwait(false);

            //Шифр 4
            if (responseEvent is GetCorrFuncResponse)
            {
                var answer = responseEvent as GetCorrFuncResponse;
                GetCorrFuncUpdate?.Invoke(answer);
            }

            //Шифр 10
            if (responseEvent is CoordsMessage)
            {
                var answer = responseEvent as CoordsMessage;
                GetCoordsMessageUpdate?.Invoke(answer);
            }

            //Шифр 11
            if (responseEvent is ModeMessage)
            {
                var answer = responseEvent as ModeMessage;
                GetExtraordinaryModeMessageUpdate?.Invoke(answer);
            }

            //Шифр 16
            if (responseEvent is TausMessage)
            {
                var answer = responseEvent as TausMessage;
                GetTausMessageUpdate?.Invoke(answer);
            }

            if (responseEvent == null)
            {
                Console.WriteLine("AWP: responseEvent == null");
            }
        }

        private async Task<TaskCompletionSource<IBinarySerializable>> SendRequest(MessageHeader header, byte[] message)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                var receiveTask = new TaskCompletionSource<IBinarySerializable>();
                if (!concurrentDictionary.ContainsKey(header.Code))
                {
                    concurrentDictionary[header.Code] = new ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>();
                }
                concurrentDictionary[header.Code].Enqueue(receiveTask);

                int count = message.Count();
                try
                {
                    await client.GetStream().WriteAsync(message, 0, count).ConfigureAwait(false);
                    if (IsWrite != null)
                        IsWrite(true);
                }
                catch (Exception)
                {
                    if (IsWrite != null)
                        IsWrite(false);
                    receiveTask.SetException(new Exception("No connection"));
                }

                return receiveTask;
            }
        }

        //Просто отправить
        private async Task SendRequestMini(MessageHeader header, byte[] message)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                int count = message.Count();
                try
                {
                    await client.GetStream().WriteAsync(message, 0, count).ConfigureAwait(false);
                    if (IsWrite != null)
                        IsWrite(true);
                }
                catch (Exception)
                {
                    if (IsWrite != null)
                        IsWrite(false);
                }
            }
        }


        //Шифр 1 
        public async Task<ModeMessage> SetMode(DapServerMode mode)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 1, length: ModeMessage.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = ModeMessage.ToBinary(header, mode);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                ModeMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as ModeMessage;
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 1, 1, 0);
                ModeMessage answer = new ModeMessage(header, DapServerMode.Stop);
                return answer;
            }
        }

        //Шифр 2
        public async Task<DefaultMessage> SetFrequency(int Frequency, int Bandwidth, byte Gain)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(2, 4 + 4 + 1);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = FrequencyMessage.ToBinary(header, Frequency, Bandwidth, Gain);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 2, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 3
        public async Task<GetSpectrumResponse> GetSpectrum(byte PostNumber, ChannelPicker Picker, double MinFrequencyMHz, double MaxFrequencyMHz, int PointCount)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(3, 1 + 1 + 4 + 4 + 4);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = GetSpectrumRequest.ToBinary(header, PostNumber, Picker, (float)MinFrequencyMHz, (float)MaxFrequencyMHz, PointCount);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetSpectrumResponse;
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 3, 1, 0);
                float[] Spectrum = new float[0];
                GetSpectrumResponse answer = new GetSpectrumResponse(header, 0, ChannelPicker.Channel1, 0, 0, Spectrum);
                return answer;
            }
        }
        public async Task<GetSpectrumResponse> GetSpectrumAwaiter(byte PostNumber, ChannelPicker Picker, double MinFrequencyMHz, double MaxFrequencyMHz, int PointCount)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(3, 1 + 1 + 4 + 4 + 4);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = GetSpectrumRequest.ToBinary(header, PostNumber, Picker, (float)MinFrequencyMHz, (float)MaxFrequencyMHz, PointCount);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);

                var answer = await Awaiter<GetSpectrumResponse>(taskCompletionSource.Task, _AwaiterTime);
                answer.Header = CheckCode(12, answer.Header);
                return answer;

            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 3, 1, 0);
                float[] Spectrum = new float[0];
                GetSpectrumResponse answer = new GetSpectrumResponse(header, 0, ChannelPicker.Channel1, 0, 0, Spectrum);
                return answer;
            }
        }

        //Шифр 4
        public async Task<DefaultMessage> GetCorrFunc(byte FuncNumber, double MinFrequencyMHz, double MaxFrequencyMHz)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(4, 1 + 4 + 4);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = GetCorrFuncRequest.ToBinary(header, FuncNumber, (float)MinFrequencyMHz, (float)MaxFrequencyMHz);

                await SendRequest(header, message).ConfigureAwait(false);

                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 4, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 5
        public async Task<ModeMessage> GetMode()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 5, length: 0);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = DefaultMessage.ToBinary(header);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                var answer = await taskCompletionSource.Task.ConfigureAwait(false) as ModeMessage;
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 5, 1, 0);
                ModeMessage answer = new ModeMessage(header, 0);
                return answer;
            }
        }

        //Шифр 6
        public async Task<FiltersMessage> SetFilters(short Threshold, ThresholdType Type)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(6, 2 + 1);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = FiltersMessage.ToBinary(header, Threshold, Type);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                FiltersMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as FiltersMessage;
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 6, 1, 0);
                FiltersMessage answer = new FiltersMessage(header, 0, 0);
                return answer;
            }
        }

        //Шифр 7
        public async Task<FiltersMessage> GetFilters(ThresholdType Type)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(7, 2 + 1);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = FiltersMessage.ToBinary(header, 0, Type);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                var answer = await taskCompletionSource.Task.ConfigureAwait(false) as FiltersMessage;
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 7, 1, 0);
                FiltersMessage answer = new FiltersMessage(header, 0, 0);
                return answer;
            }
        }

        //Шифр 8
        public async Task<ApplyExFilterMessage> ApplyExFilter(bool value)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(8, 1);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = ApplyExFilterMessage.ToBinary(header, value);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                ApplyExFilterMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as ApplyExFilterMessage;
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 8, 1, 0);
                ApplyExFilterMessage answer = new ApplyExFilterMessage(header,false);
                return answer;
            }
        }

        /// <summary>
        /// Шифр 9
        /// </summary>
        /// <param name="RecordStartStop bool value"></param>
        /// <returns></returns>
        public async Task<ToggleStartStopMessage> ToggleStartStop(bool value, ToggleMode toggleMode)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(9, 1 + 1);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = ToggleStartStopMessage.ToBinary(header, value, toggleMode);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                ToggleStartStopMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as ToggleStartStopMessage;
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 9, 1, 0);
                ToggleStartStopMessage answer = new ToggleStartStopMessage(header, false, toggleMode);
                return answer;
            }
        }

        //Шифр 10 - см. Events

        //Шифр 11 - см. Events

        //Шифр 12
        public async Task<DeviceGainMessage> SetDeviceGain(DapDevice Device, byte EPO, byte Gain)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(12, 1 + 1 + 1);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = DeviceGainMessage.ToBinary(header, Device, EPO, Gain);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                DeviceGainMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DeviceGainMessage;
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 12, 1, 3);
                DeviceGainMessage answer = new DeviceGainMessage(header, 0, 0, 0);
                return answer;
            }
        }
        
        //Шифр 13
        public async Task<DeviceGainMessage> GetDeviceGain(DapDevice Device, byte EPO)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(13, 1 + 1 + 1);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = DeviceGainMessage.ToBinary(header, Device, EPO, 0);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                DeviceGainMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DeviceGainMessage;
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 13, 1, 3);
                DeviceGainMessage answer = new DeviceGainMessage(header, 0, 0, 0);
                return answer;
            }
        }

        //Шифр 14
        public async Task<DefaultMessage> SendRangeMessage(double StartRangeMHz, double EndRangeMHz, RangeType Type)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(14, 8 + 8 + 1);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = RangeMessage.ToBinary(header, StartRangeMHz, EndRangeMHz, Type);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 14, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 15
        public async Task<DefaultMessage> SendSignalMessage(int ID, double FrequencykHz, float BandwidthkHz, RangeType RType, TableType TType)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(15, 4 + 8 + 4 + 1 + 1);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = SignalMessage.ToBinary(header, ID, FrequencykHz, BandwidthkHz, RType, TType);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 15, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 16 - см. Events

        //Ширф 17
        public async Task<DefaultMessage> SendExtraMessage(ExtraCode extraCode)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(17, 1);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = ExtraMessage.ToBinary(header, extraCode);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 17, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        private MessageHeader CheckCode(byte Code, MessageHeader answerHeader)
        {
            if (answerHeader.Code == 0)
            {
                return new MessageHeader(0, 0, Code, 5, 0);
            }
            else return answerHeader;
        }

        private async Task<T> Ws<T>(int timeout) where T : class, IBinarySerializable, new()
        {
            await Task.Delay(timeout);
            T temp = new T();
            return temp;
        }

        private async Task<T> Awaiter<T>(Task<IBinarySerializable> task, int timeout) where T : class, IBinarySerializable, new()
        {
            var task2 = Ws<T>(timeout);

            var answer = new T();

            if (await Task.WhenAny(task, task2) == task)
            {
                answer = task.Result as T;
                //Console.WriteLine("task completed within timeout");
            }
            else
            {
                answer = task2.Result as T;
                //Console.WriteLine("timeout logic");
            }
            return answer;
        }
    }
}
